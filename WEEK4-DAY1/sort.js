let data = [
    {
        name: "Jedidiah",
        umur: 24,
    },
    {
        name: "Angela",
        umur: 22,        
    },
    {
        name: "Maulana",
        umur: 24,
    },
    {
        name: "Aditya",
        umur: 24,
    },
    {
        name: "Erdia",
        umur: 20,
    },
    {
        name: "Basnur",
        umur: 30,
    },
    {
        name: "Jadhuk",
        umur: 20,
    },
    {
        name: "Dollyton",
        umur: 23,
    },
    {
        name: "August",
        umur: 25,
    },
    {
        name: "Yenni",
        umur: 27,
    }
];

let sortBy = [{
    prop:'umur',
    direction: -1
  },{
    prop:'name',
    direction: 1
  }];

data.sort(function(a, b){
    let i = 0, result = 0;
    while(i < sortBy.length && result === 0) {
      result = sortBy[i].direction *    (a[ sortBy[i].prop ].toString() < b[ sortBy[i].prop ].toString() ? -1 :
                                        (a[ sortBy[i].prop ].toString() > b[ sortBy[i].prop ].toString() ? 1 : 0));
      i++;
    }
    return result;
  })

// data.sort((a, b) => {
    
//     if(a.umur && !b.umur){
//         return -1;
//     } else if(!a.umur && b.umur){
//         return 1;
//     } else{
//         return b.name - a.name;
//     }
// });

// console.log(data);