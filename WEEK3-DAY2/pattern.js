let n = 7;
let downwardTriLeft = "";
for (let i = 0; i < n; i++) {
  for (let k = 0; k < n - i; k++) {
    downwardTriLeft += "*";
  }
  downwardTriLeft += "\n";
}
console.log(downwardTriLeft);



let n = 7;
let downwardTriRi = "";
for (let i = 0; i < n; i++) {
    for (let j = 1; j < i + 1; j++)
        downwardTriRi += " ";
    for (let k = 0; k < n - i; k++) {
        downwardTriRi += "*";
    }
    downwardTriRi += "\n";
}
console.log(downwardTriRi);



let n = 7;
let pyramid  = "";

for (let i = 1; i <= n; i++) {
  for (let j = 1; j <= n - i; j++) {
    pyramid += " ";
  }
  for (let k = 0; k < 2 * i - 1; k++) {
    pyramid += "*";
  }
  pyramid += "\n";
}
console.log(pyramid);



let n = 7;
let reversePyramid = "";

for (let i = 0; i < n; i++) {
  for (let j = 0; j < i; j++) {
    reversePyramid += " ";
  }
  for (let k = 0; k < 2 * (n-i) - 1; k++) {
    reversePyramid += "*";
  }
  reversePyramid += "\n";
}
console.log(reversePyramid);